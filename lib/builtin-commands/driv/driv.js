"use strict";
let fsAutocomplete = require( 'vorpal-autocomplete-fs' );

module.exports = function ( cli, options ) {
    cli
        .command( 'update', 'Updates the core and plugins. Use -c to update core only' )
        .option( '-v, --verbose', 'Verbose output' )
        .option( '-c, --core', 'Updates the core only' )
        .action( function ( args, callback ) {
            Driv.remove_commands();
            if ( args.options.core ) {
                Driv.update_core();
            }
            else {
                Driv.update_core();
                Driv.update_commands();

            }
            callback();
        } );

    cli
        .command( 'info', 'Displays info about the installation' )
        .action( function ( args, callback ) {
            Driv.log( '  Repositories: ' );
            for ( let repo of Driv.get_repos().repos ) {
                Driv.log( '    ' + repo.url );
            }
            callback();
        } );

    cli
        .command( 'contact', 'Contact info' )
        .action( function ( args, callback ) {
            Driv.log( cat( __dirname + '/data/contact.md' ) );
            callback();
        } );

    cli
        .command( 'pwd', 'Display current working directory' )
        .action( function ( args, callback ) {
            Driv.log( cli.chalk.bold( Driv.get_current_working_dir() ) );
            callback();
        } );

    cli
        .command( 'cd [dirs...]', 'Change working directory' )
        .autocomplete( fsAutocomplete( {directory: true} ) )
        .action( function ( args, callback ) {
            if ( args.dirs && args.dirs[0] ) {
                cd( args.dirs[0] );
                Driv.set_current_working_dir( pwd() );
            }
            callback();
        } );

    cli
        .command( 'ls [dirs...]', 'List contents directory' )
        .option( '-l, --long', 'Long listing format' )
        .option( '-a, --all', 'All files including dot files' )
        .autocomplete( fsAutocomplete( {directory: true} ) )
        .action( function ( args, callback ) {
            let dir = args.dirs && args.dirs[0] ? args.dirs[0] : '.';
            let opts = '';
            if (args.options.long) {
                opts += 'l';
            }
            if ( args.options.all ) {
                opts += 'a';
            }
            if (opts.length > 0) {
                opts = '-'+ opts;
            }
            Driv.log(exec('ls ' + opts + ' ' + dir, {silent:true}).output);
            //
            //let files = ls( opts, dir );
            //for ( let file of files ) {
            //    let str = ' ';
            //    if ( long ) {
            //        str += Utils.format_date( file.mtime, 'MMM DD HH:MM' ) + '  ';
            //    }
            //    if ( test( '-d', file.name ) ) {
            //        str += cli.chalk.bold( file.name );
            //    }
            //    else {
            //        str += file.name;
            //    }
            //    Driv.log( str );
            //}
            callback();
        } );

    cli
        .command( 'cat [file]', 'Output contents of a file' )
        .autocomplete( fsAutocomplete( {directory: false} ) )
        .action( function ( args, callback ) {
            if ( args.file ) {
                if ( test( '-f', args.file ) ) {
                    Driv.log( cat( args.file ) );
                }
            }
            callback();
        } );

};
