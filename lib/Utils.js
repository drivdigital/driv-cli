"use strict";

class Utils {

    static merge_recursive( obj1, obj2 ) {
        for ( var p in obj2 ) {
            try {
                // Property in destination object set; update its value.
                if ( obj2[p].constructor == Object ) {
                    obj1[p] = MergeRecursive( obj1[p], obj2[p] );

                } else {
                    obj1[p] = obj2[p];

                }

            } catch ( e ) {
                // Property in destination object not set; create it and set its value.
                obj1[p] = obj2[p];

            }
        }

        return obj1;

    }

    static get_last_part_of_path( path ) {
        return path.replace( /^.*[\\\/]/, '' );
    }

    /**
     * @param {Date} date
     * @returns {string}
     */
    static format_date( date, format ) {
        let moment = require('moment');
        let d = moment(date);
        return d.format(format);

    }
}

module.exports = Utils;