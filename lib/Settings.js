"use strict";

require( 'shelljs/global' );
var Utils = require( './Utils' );

var _defaults = {
    delimiter: {
        color: 'reset'
    }
};

var _settings = {};

class Settings {
    static init( path_to_settings ) {

        if ( test( '-f', path_to_settings ) ) {
            try {
                // Merge properties from user defined settings
                var user_settings = JSON.parse( (cat( path_to_settings )) );
                _settings = Utils.merge_recursive( _defaults, user_settings );

            }
            catch ( ex ) {
                console.log( 'Error in ' + path_to_settings, ex );
                _settings = _defaults;
            }
        } else {
            _settings = _defaults;
        }

    }

    static get_settings() {
        return _settings;
    }
}

module.exports = Settings;