"use strict";

class Git {
    constructor( silent ) {
        this.silent = silent;
    }

    get_status() {
        return this._exec( 'git status' );
    }

    pull() {
        return this._exec( 'git pull' );
    }

    is_repo_clean() {
        return this.get_status().toLowerCase().indexOf( 'working directory clean' ) > -1;
    }

    _exec( git_cmd ) {
        return exec( git_cmd, {silent: this.silent} ).output.trim();
    }
}

module.exports = Git;