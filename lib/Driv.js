"use strict";

let Git = require( './Git' );

let loaded_commands = [];

class Driv {

    static init_commands() {
        cd( DRIV_HOME );
        if ( ! test( '-f', REPOSITORY_JSON_FILE ) ) {
            Driv.log( REPOSITORY_JSON_FILE + ' not found' );
            exit( 1 );
        }

        let repos = Driv.get_repos().repos;
        let repos_found_count = 0;
        for ( let repo of repos ) {
            let name = Utils.get_last_part_of_path( repo.url ).split( '.' )[0];
            let dir = COMMANDS_DIR + '/' + name;
            if ( test( '-d', dir + '/.git' ) ) {
                repos_found_count ++;
            }
        }
        if ( repos_found_count < repos.length ) {
            Driv.update_commands();
        }
        else {
            Driv.reload_commands();
        }
    }

    static update_commands() {
        let Utils = require( './Utils' );
        let silent = true;

        cd( DRIV_HOME );

        if ( ! test( '-d', COMMANDS_DIR ) ) {
            mkdir( '-p', COMMANDS_DIR );
        }

        for ( let repo of Driv.get_repos().repos ) {
            let url = repo.url;
            let name = Utils.get_last_part_of_path( url ).split( '.' )[0];
            let dir = COMMANDS_DIR + '/' + name;

            if ( ! test( '-d', dir + '/.git' ) ) {
                mkdir( '-p', dir );
                cd( dir );
                Driv.log( '  Cloning commands from ' + url );
                exec( 'git clone ' + url + ' .', {silent: silent} );
                if ( test( '-f', 'package.json' ) ) {
                    Driv.log( '  Pulling dependencies' );
                    exec( 'npm install', {silent: silent} );
                }
                else {
                    Driv.log( '  No dependencies found' );
                }
            }
            else {
                Driv.log( '  Pulling commands from ' + url );
                cd( dir );
                exec( 'git pull', {silent: silent} );
                if ( test( '-f', 'package.json' ) ) {
                    Driv.log( '  Updating dependencies' );
                    exec( 'npm update', {silent: silent} );
                }
                else {
                    Driv.log( '  No dependencies found' );
                }
            }
            Driv.log('');
            cd( DRIV_HOME );
        }

        Driv.reload_commands( true );
    }

    static get_repos() {
        cd( DRIV_HOME );
        if ( ! test( '-f', REPOSITORY_JSON_FILE ) ) {
            return null;
        }
        return JSON.parse( cat( REPOSITORY_JSON_FILE ) );
    }

    /**
     * Reloads the commands in the commands/ directory.
     * When refresh all is false, only non-loaded commands are loaded.
     */
    static reload_commands() {

        let source_dirs = [];

        source_dirs.push( DRIV_HOME + '/lib/builtin-commands/*' );
        for ( let d of ls( '-d', COMMANDS_DIR + '/*' ) ) {
            source_dirs.push( d + '/*' );
        }

        let command_dirs = ls( '-d', source_dirs );

        loaded_commands = [];

        for ( var cmd_dir of command_dirs ) {
            let command_name = Utils.get_last_part_of_path( cmd_dir );

            if ( ! test( '-d', cmd_dir ) ) {
                continue;
            }

            if ( RESERVED_COMMAND_NAMES.indexOf( command_name ) > - 1 ) {
                continue;
            }

            // @todo: make safer
            if ( loaded_commands.indexOf( command_name ) > - 1 ) {
                continue;
            }

            //if ( ! refresh_all ) {
            //    if ( loaded_commands.indexOf( name ) > - 1 ) {
            //        continue;
            //    }
            //}

            let index_file = cmd_dir + '/' + command_name + '.js';
            delete require.cache[require.resolve( index_file )];
            require( index_file )( vorpal );

            loaded_commands.push( command_name );
        }
    }

    /**
     * Returns true if a command exist.
     *
     * @param {String} command_name
     * @returns {boolean}
     * @todo: should traverse vorpal commands, not the commands directory
     */
    static command_exist( command_name ) {
        let exist = false;
        let commands = ls( '-d', [COMMANDS_DIR + '/*', DRIV_HOME + '/lib/builtin-commands/*'] );
        for ( var cmd of commands ) {
            let name = cmd.substr( cmd.lastIndexOf( '/' ) + 1 );
            if ( name == command_name ) {
                exist = true;
                break;
            }
        }
        return exist;
    }

    /**
     * Returns true if the command has a .gitignore file
     *
     * @param {String} command_name
     * @returns {*}
     */
    static is_command_git_ignored( command_name ) {
        let cmd_dir = COMMANDS_DIR + '/' + command_name;
        if ( ! test( '-d', cmd_dir ) ) {
            return false;
        }
        return test( '-f', cmd_dir + '/.gitignore' );
    }

    /**
     * Restarts the driv shell.
     */
    static restart() {
        vorpal.commands.forEach( function ( cmd ) {
            if ( RESERVED_COMMAND_NAMES.indexOf( cmd._name ) == - 1 ) {
                cmd.remove();
            }
        } );
        Driv.reload_commands( true );
    }

    static remove_commands() {
        vorpal.commands.forEach( function ( cmd ) {
            if ( RESERVED_COMMAND_NAMES.indexOf( cmd._name ) == - 1 ) {
                cmd.remove();
            }
        } );
    }

    /**
     * Updates either the core or the commands.
     * @todo: refactor/move.
     *
     * @param args vorpal arguments
     */
    static update_core( args ) {
        var org_dir = pwd();
        cd( DRIV_HOME );

        var git = new Git( args.options.verbose );

        if ( ! git.is_repo_clean() ) {
            Driv.log( 'Can not fetch code. Local repo is not clean.' );
            return;
        }

        Driv.log( 'Pulling from remote repo' );
        git.pull();

        try {
            Driv.log( 'Fetching dependencies' );
            exec( 'npm install' );
        }
        catch ( ex ) {
            Driv.log( "Something went wrong fetching dependencies. You could try to run 'npm cache clean'", ex );
        }

        Driv.log( 'Restarting' );
        cd( org_dir );
        Driv.restart();
    }

    static has_alias( file ) {
        let re = /^alias\s*?\w+\s*?=\s*?['"][\w\s\/-]*\/cli['"]/gmi;
        let content = cat(file);
        return re.exec(content) != null;
    }

    static get_home_dir() {
        return process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'];
    }

    static log( msg ) {
        vorpal.log( msg );
    }

    static warn( msg ) {
        vorpal.log( vorpal.chalk.bold( msg ) );
    }

    static error( msg ) {
        vorpal.log( vorpal.chalk.bold( msg ) );
    }

    static set_current_working_dir( path ) {
        cd( path );
        Driv.set_delimiter( path );
    }

    static get_current_working_dir( ) {
        return pwd();
    }

    static set_delimiter( path ) {
        let color = Settings.get_settings().delimiter.color;
        let bold = global.vorpal.chalk[color]['bold'];
        global.vorpal.delimiter(bold( 'drivdigital [~/' + Utils.get_last_part_of_path( path ) + '] >' ));
    }
}

module.exports = Driv;
