import test from 'ava';

require( '../global' );

test( 'Test that shelljs/global is present', t => {
    t.true( typeof exec == 'function' );
} );

test( 'global.DRIV_HOME should exist and be of type string', t => {
    t.true( typeof global.DRIV_HOME == 'string' );
} );

test( 'global.COMMANDS_DIR should exist and be of type string', t => {
    t.true( typeof global.COMMANDS_DIR == 'string' );
} );

test( 'global.RESERVED_COMMAND_NAMES should exist and be of type array', t => {
    t.true( Array.isArray( global.RESERVED_COMMAND_NAMES ) );
} );

test( 'global.Driv should exist and be of type function', t => {
    t.true( typeof global.Driv == 'function' );
} );

test( 'global.version should exist and be of type string', t => {
    t.true( typeof global.version == 'string' );
} );

test( 'global.vorpal should exist and be of type object', t => {
    t.true( typeof global.vorpal == 'object' );
} );

