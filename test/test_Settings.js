import test from 'ava';

var Settings = require( '../lib/Settings' );

test( 'Settings should exist and be of type object', t => {
    t.true( typeof Settings == 'function' );
} );

test( 'Test that the user settings is applied', t => {
    Settings.init( __dirname + '/fixtures/.driv.json' );
    let result = Settings.get_settings();
    t.same( result.delimiter.color, 'reset' );
    t.same( result.a, 'Hello, World!' );
    t.same( result.b.b2, true );

} );

