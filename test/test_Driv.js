import test from 'ava';

//@todo: it should not be necessary to include global here
require( '../global' );

test( 'Driv.update_commands() should exist and be of type function', t => {
    t.true( typeof Driv.update_commands == 'function' );
} );

test( 'Driv.reload_commands() should exist and be of type function', t => {
    t.true( typeof Driv.reload_commands == 'function' );
} );

test( 'Driv.command_exist() should exist and be of type function', t => {
    t.true( typeof Driv.command_exist == 'function' );
} );

test( 'Test that the driv command exist', t => {
    t.plan( 1 );
    t.true( Driv.command_exist( 'driv' ) );
} );

test( 'Driv.is_command_git_ignored() should exist and be of type function', t => {
    t.true( typeof Driv.is_command_git_ignored == 'function' );
} );

test( 'Driv.restart() should exist and be of type function', t => {
    t.true( typeof Driv.restart == 'function' );
} );

test( 'Driv.update() should exist and be of type function', t => {
    t.true( typeof Driv.update_core == 'function' );
} );

test( 'Driv.log() should exist and be of type function', t => {
    t.true( typeof Driv.log == 'function' );
} );

test( 'Driv.warn() should exist and be of type function', t => {
    t.true( typeof Driv.warn == 'function' );
} );

test( 'Driv.has_alias() should exist and be of type function', t => {
    t.true( typeof Driv.has_alias == 'function' );
} );

test( 'Driv.has_alias() should find an alias in the given file', t => {
    t.true( Driv.has_alias( 'fixtures/.bash_profile1' ) );
} );

test( 'Driv.has_alias() should find an alias in the given file', t => {
    t.true( Driv.has_alias( 'fixtures/.bash_profile2' ) );
} );

test( 'Driv.has_alias() should not find an alias in the given file', t => {
    t.false( Driv.has_alias( 'fixtures/.bash_profile3' ) );
} );

test( 'Driv.has_alias() should find an alias to cli regardless of alias path', t => {
    t.true( Driv.has_alias( 'fixtures/.bash_profile4' ) );
} );

test( 'Driv.get_home_dir() should exist and be of type function', t => {
    t.true( typeof Driv.get_home_dir == 'function' );
} );

test( 'Driv.get_home_dir() should return a string', t => {
    t.true( typeof Driv.get_home_dir() == 'string' );
} );
