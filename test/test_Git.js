import test from 'ava';

let Git = require( '../lib/Git' );

test( 'Git should exist and be of type function', t => {
    t.true( typeof Git == 'function' );
} );
