import test from 'ava';

var Utils = require( '../lib/Utils' );

test( 'mergeRecursive() should exist and be of type function', t => {
    t.true( typeof Utils.merge_recursive == 'function' );
} );

test( 'mergeRecursive() should merge two object literals', t => {

    let obj1 = {a: 1, b: 2, c: 3};
    let obj2 = {d: 4, e: 5, f: 6, g: {a: 1}};
    let merged = Utils.merge_recursive( obj1, obj2 );

    t.plan( 3 );

    t.ok( merged.g.a );
    t.same( merged.g.a, 1 );
    t.same( merged.e, 5 );
} );

test( 'get_last_part_of_path() should exist and be of type function', t => {
    t.true( typeof Utils.get_last_part_of_path == 'function' );
} );

test( 'get_last_part_of_path() should return the last part of a given path', t => {

    let result;

    t.plan( 4 );

    result = Utils.get_last_part_of_path( '/a/b/c/d' );
    t.same( result, 'd' );

    result = Utils.get_last_part_of_path( 'https://www.drivdigital.no/prosjektleder-sokes-til-driv-digital' );
    t.same( result, 'prosjektleder-sokes-til-driv-digital' );

    result = Utils.get_last_part_of_path( 'https://www.drivdigital.no/prosjektleder-sokes-til-driv-digital/' );
    t.same( result, '' );

    result = Utils.get_last_part_of_path( 'https://www.drivdigital.no/downloads/foo.1.8.9.zip' );
    t.same( result, 'foo.1.8.9.zip' );
} );