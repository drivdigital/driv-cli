"use strict";

require( 'shelljs/global' );

global.DRIV_HOME = __dirname;
global.REPOSITORY_JSON_FILE = __dirname + '/repositories.json';
global.COMMANDS_REPO = 'git@bitbucket.org:drivdigital/driv-cli-commands.git';
global.COMMANDS_DIR = __dirname + '/commands';
global.RESERVED_COMMAND_NAMES = ['README.md', 'help', 'exit', '.idea', '.log', 'node_modules', 'package.json', 'npm-debug.log'];

let Vorpal = require( 'vorpal' );
global.vorpal = new Vorpal();

global.Settings = require( './lib/Settings' );
global.Driv = require( './lib/Driv' );
global.Utils = require( './lib/Utils' );
global.version = require( './package.json' ).version;
