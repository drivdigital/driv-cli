**Work in progress**

# Driv Cli

Driv cli is a shell for tools used by Driv Digital

## Install

Before installing, you need to verify that you have Node.js installed.

1. `git clone git@bitbucket.org:drivdigital/driv-cli.git`
2. `cd driv-cli` 
3. `npm install`

In order to install commands you need a `repositories.json` that should look like this


    {
      "repos": [
        {
          "url": "..."
        }
      ]
    }


Each repo in repos should contain a valid git url to a project with commands.  
Example:

    {
      "repos": [
        {
          "url": "git@bitbucket.org:drivdigital/driv-cli-wp.git"
        }
      ]
    }

## Run

`./cli`

driv-cli has a few default commands by default. Other commands are loaded from the `repositories.json` configuration.

Some useful built-in commands

* `help` for help.  
* `help <command>` for help on a specific command.  
* `driv|d info` displays information about the configuration etc.  
* `driv|d update` utdates the core and the repositories.

## Hacking

### Testing

Run `npm test` from the project root.

driv-cli uses the AVA test framework. For more information about AVA, please visit: https://github.com/sindresorhus/ava


### Creating a new command

TBD!

#### Existing repository

TBD!

#### New repository

TBD!


    /**
      * @param {Object} cli - Driv's Vorpal instance.
      * @param {Object} options - Not used at the moment.
      */
    module.exports = function ( cli, options ) {
        cli
           .command( '<command_name>', 'Description' )
           .action( function ( args, callback ) {
               // Code here
               some_function( args );
               // Always call the callback function in order to return to the driv-cli.
               callback();
           } );
    };

    function some_function( args ) {
        Driv.log( args );
    }

See https://github.com/dthree/vorpal/wiki/api-|-vorpal.command for how to use the vorpal command.
See global.js for additional objects like `driv` and `util`.
